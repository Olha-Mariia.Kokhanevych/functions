const getSum = (str1, str2) => {
  const reg = /^\d+$/;
  if (typeof str1 === "string" && typeof str2 === "string") {
    if (reg.test(str1) && reg.test(str2)) {
      const result = Number(str1) + Number(str2);
      return String(result);
    } else if (str1 === "" && reg.test(str2)) {
      const result = Number(str1) + Number(str2);
      return String(result);
    } else if (str2 === "" && reg.test(str1)) {
      const result = Number(str1) + Number(str2);
      return String(result);
    } else {
      return false;
    }
  } else {
    return false;
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let aut = 0,
    com = 0;
  listOfPosts.forEach(({ author, comments }) => {
    if (author === authorName) {
      aut++;
    }
    if (comments)
      com += comments.filter(({ author }) => author === authorName).length;
  });
  return `Post:${aut},comments:${com}`;
};

const tickets = (people) => {
  var a25 = 0,
    a50 = 0;
  for (var i = 0; i < people.length; i++) {
    if (people[i] == 25) {
      a25 += 1;
    }
    if (people[i] == 50) {
      a25 -= 1;
      a50 += 1;
    }
    if (people[i] == 100) {
      if (a50 == 0 && a25 >= 3) {
        a25 -= 3;
      } else {
        a25 -= 1;
        a50 -= 1;
      }
    }
    if (a25 < 0 || a50 < 0) {
      return "NO";
    }
  }
  return "YES";
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
